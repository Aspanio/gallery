document.addEventListener('DOMContentLoaded', function() {

    function HighLighter () {}

    HighLighter.prototype.tracker = () => {

        arrayOfImages.reduce((acc, current) => {
            current.addEventListener('mouseover', () => {
                main.setAttribute('src', current.src)
                current.style.boxShadow = "0 0 5px #000000"
            })
            current.addEventListener    ('mouseout', () => {
                current.style.boxShadow = "none"
            })
        }, '')
    }  

    let main = document.querySelector('.main');
    let images = document.querySelectorAll('img:not(.main)')
    let arrayOfImages = Array.prototype.slice.call(images)

    let obj = new HighLighter();
    obj.tracker();
});
